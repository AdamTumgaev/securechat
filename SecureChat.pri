CONFIG(release, debug|release): DESTDIR = $$PWD/Release
CONFIG(debug  , debug|release): DESTDIR = $$PWD/Debug

OBJECTS_DIR = $$DESTDIR/.obj/$$TARGET
MOC_DIR     = $$DESTDIR/.moc/$$TARGET
RCC_DIR     = $$DESTDIR/.rcc/$$TARGET
UI_DIR      = $$DESTDIR/.ui/$$TARGET

DEFINES += "DATA_STREAM_VERSION=QDataStream::Qt_5_12" "SECURE_CHAT_PORT=443"
