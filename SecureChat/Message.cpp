#include "Message.hpp"

#include <QVector>
#include <QMetaType>
#include <QHash>
#include <QString>
#include <QJsonObject>
#include <QJsonValue>

static struct MessageTypes
{
	public:
		MessageTypes();

		static QString		 toString  (int);
		static Message::Type fromString(const QString &);

	private:
		QVector<QString>				typeToString;
		QHash  <QString, Message::Type> stringToType;
} messageTypes;

MessageTypes::MessageTypes()
{
	//Insert the types as string
	typeToString =
	{
		QStringLiteral("login"),
		QStringLiteral("login-status"),
		QStringLiteral("logout"),
		QStringLiteral("message")
	};

	//Reserve the same size for the string type as the normal enum type
	qint32 size = typeToString.size();
	stringToType.reserve(size);

	//Insert all to the enum hash
	for(qint32 i = 0; i < size; i++)
		stringToType.insert(typeToString.at(i), static_cast<Message::Type>(i));

	//Register the message pointer
	qRegisterMetaType<MessagePointer>("MessagePointer");
}

inline QString MessageTypes::toString(int type)
{
	//If the type is valid, return the stored string for this type
	return (type < 0 || type >= messageTypes.typeToString.size()) ? QString() : messageTypes.typeToString.at(type);
}

inline Message::Type MessageTypes::fromString(const QString &string)
{
	//If the string is valid, return the enum type, else return as unknown type
	return messageTypes.stringToType.value(string, Message::UnknownType);
}

//---------------------------------------------------------------------------------------------------------------------------------------- //

Message::Type Message::getType(const QJsonObject &json)
{
	//Get the stored type of the json object and return the enum type
	QString messageType = json.value(QStringLiteral("type")).toString();
	return MessageTypes::fromString(messageType);
}

Message::~Message()
{
}

void Message::setUsername(const QString &username)
{
	this->username = username;
}

QString Message::getUsername() const
{
	return username;
}

bool Message::fromJson(const QJsonObject &json)
{
	//The child classes have to return the type that is in the json object
	Q_ASSERT(getType() == getType(json));

	//Save the name from the object and check if it is null
	username = json.value(QStringLiteral("username")).toString();
	return !username.isEmpty();
}

QJsonObject Message::toJson() const
{
	//Convert the message to a json object
	QJsonObject result =
	{
		{ QStringLiteral("type")    , MessageTypes::toString(getType()) },
		{ QStringLiteral("username"), username }
	};

	return result;
}

//---------------------------------------------------------------------------------------------------------------------------------------- //

Message::Type MessageLogin::getType() const
{
	return LoginType;
}

Message *MessageLogin::clone() const
{
	return new MessageLogin(*this);
}

//---------------------------------------------------------------------------------------------------------------------------------------- //

MessageLoginStatus::MessageLoginStatus()
	: status(Success)
{}

void MessageLoginStatus::setStatus(Status status)
{
	this->status = status;
}

MessageLoginStatus::Status MessageLoginStatus::getStatus() const
{
	return status;
}

void MessageLoginStatus::setErrorText(const QString &errorText)
{
	this->errorText = errorText;
}

QString MessageLoginStatus::getErrorText() const
{
	return errorText;
}

bool MessageLoginStatus::fromJson(const QJsonObject &json)
{
	//Check the json file (if type is correct and there is a username)
	if(!Message::fromJson(json))
		return false;

	//Get the saved status and error text
	status    = static_cast<Status>(json.value(QStringLiteral("status")).toInt());
	errorText = json.value(QStringLiteral("errorText")).toString();

	//Check if message was successfull or if server did not set any error text (like username already taken or so)
	return status == Success || (status == Fail && !errorText.isEmpty());
}

QJsonObject MessageLoginStatus::toJson() const
{
	//Create a json object and insert the status
	QJsonObject json = Message::toJson();
	json.insert(QStringLiteral("status"), static_cast<int>(status));

	//Insert the error into the json object if the status is failed
	if(status == Fail)
		json.insert(QStringLiteral("errorText"), errorText);

	return json;
}

Message::Type MessageLoginStatus::getType() const
{
	return LoginStatusType;
}

Message *MessageLoginStatus::clone() const
{
	return new MessageLoginStatus(*this);
}

//---------------------------------------------------------------------------------------------------------------------------------------- //

Message::Type MessageLogout::getType() const
{
	return LogoutType;
}

Message * MessageLogout::clone() const
{
	return new MessageLogout(*this);
}

//---------------------------------------------------------------------------------------------------------------------------------------- //

void MessageText::setText(const QString &text)
{
	message = text;
}

QString MessageText::getText() const
{
	return message;
}

bool MessageText::fromJson(const QJsonObject &json)
{
	//Check the json file (if type is correct and there is a username)
	if(!Message::fromJson(json))
		return false;

	//Set the message and return true if message is not empty
	message = json.value(QStringLiteral("text")).toString();
	return !message.isEmpty();
}

QJsonObject MessageText::toJson() const
{
	//Insert username, type (through Message::toJson()) and them message into the json object
	QJsonObject result = Message::toJson();
	result.insert(QStringLiteral("text"), message);

	return result;
}

Message::Type MessageText::getType() const
{
	return TextType;
}

Message * MessageText::clone() const
{
	return new MessageText(*this);
}
