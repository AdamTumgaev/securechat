#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include "SecureChat.hpp"

#include <QString>
#include <QSharedData>

class QJsonObject;

class SECURECHAT_EXPORT Message : public QSharedData
{
	public:
		enum Type { UnknownType = -1, LoginType = 0, LoginStatusType, LogoutType, TextType };

	public:
		Message() = default;
		virtual ~Message();

	protected:
		Message			   (const Message &) = default;
		Message &operator= (const Message &) = default;

	public:
		static  Type getType(const QJsonObject &);
		virtual Type getType() const = 0;

		void	setUsername(const QString &);
		QString getUsername() const;

		//Serialization
		virtual bool		fromJson(const QJsonObject &);
		virtual QJsonObject toJson  () const;

	protected:
		virtual Message *clone() const = 0;

	protected:
		QString username;

	private:
		friend MessagePointer;
};

template <>
inline Message *MessagePointer::clone()
{
	return d->clone();
}

class SECURECHAT_EXPORT MessageLogin : public Message
{
	public:
		MessageLogin() = default;

	private:
		MessageLogin(const MessageLogin &) = default;

		Type getType() const override;

	protected:
		Message *clone() const override;
};

class SECURECHAT_EXPORT MessageLoginStatus : public Message
{
	public:
		enum Status { Fail, Success };

	public:
		MessageLoginStatus();

	private:
		MessageLoginStatus(const MessageLoginStatus &) = default;

	public:
		Type getType() const override;

		void   setStatus(Status);
		Status getStatus() const;

		void    setErrorText(const QString &);
		QString getErrorText() const;

		bool		fromJson(const QJsonObject &) override;
		QJsonObject toJson  () const override;

	protected:
		Message *clone() const override;

		Status  status;
		QString errorText;
};

class SECURECHAT_EXPORT MessageLogout : public Message
{
	public:
		MessageLogout() = default;

	private:
		MessageLogout(const MessageLogout &) = default;

	public:
		Type getType() const override;

	protected:
		Message *clone() const override;
};

class SECURECHAT_EXPORT MessageText : public Message
{
	public:
		MessageText() = default;

	private:
		MessageText(const MessageText &) = default;

	public:
		Type getType() const override;

		void    setText(const QString &);
		QString getText() const;

		bool		fromJson(const QJsonObject &) override;
		QJsonObject toJson  () const override;

	protected:
		Message *clone() const override;

	protected:
		QString message;
};

#endif //MESSAGE_HPP
