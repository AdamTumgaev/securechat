#ifndef SECURECHAT_HPP
#define SECURECHAT_HPP

#include <QtGlobal>

#if defined(SECURECHAT_LIBRARY)
#  define SECURECHAT_EXPORT Q_DECL_EXPORT
#else
#  define SECURECHAT_EXPORT Q_DECL_IMPORT
#endif

template <class Type>
class QSharedDataPointer;

class Message;
typedef QSharedDataPointer<Message> MessagePointer;

#endif //SECURECHAT_HPP
