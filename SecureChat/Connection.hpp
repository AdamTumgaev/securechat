#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include "SecureChat.hpp"

#include <QObject>
#include <QAbstractSocket>
#include <QPointer>
#include <QHostAddress>

class QSslSocket;
class QJsonObject;

class SECURECHAT_EXPORT Connection : public QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(Connection)

	public:
		explicit Connection(QObject *parent = nullptr);
		~Connection() override;

		QAbstractSocket::SocketState getState() const;

		QString getLastError() const;

	public slots:
		bool open (qintptr);
		bool open (QSslSocket *);
		bool open (const QHostAddress &, quint16 = 0);
		void close();

		void send(const MessagePointer &);

	private:
		void initializeSocket(QSslSocket *);
		void decodeJson		 (const QJsonObject &);

	private slots:
		void readData    ();
		void setLastError(QAbstractSocket::SocketError);

	signals:
		void opened();
		void closed();

		void received	 (const MessagePointer &);
		void statusReport(const QString &);

		void error();

	private:
		QPointer<QSslSocket> socket;
		QString				 lastError;
};

#endif //CONNECTION_HPP
