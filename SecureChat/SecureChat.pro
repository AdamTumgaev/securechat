include(../SecureChat.pri)

QT += core gui widgets network

TARGET   = SecureChat
TEMPLATE = lib

DEFINES += SECURECHAT_LIBRARY QT_DEPRECATED_WARNINGS

SOURCES += *.cpp
HEADERS += *.hpp
