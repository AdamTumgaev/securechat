#include "Connection.hpp"
#include "Message.hpp"

#include <QDataStream>
#include <QSslSocket>

#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>

Connection::Connection(QObject *parent)
	: QObject(parent)
	, socket (nullptr)
{
	//Notify that the connection is some error happened
	connect(this, &Connection::error, this, &Connection::closed);
}

Connection::~Connection()
{
	delete socket;
}

QAbstractSocket::SocketState Connection::getState() const
{
	//If the connection was newly created, return its state, else return the unconnected state
	return (socket) ? socket->state() : QAbstractSocket::UnconnectedState;
}

QString Connection::getLastError() const
{
	return lastError;
}

bool Connection::open(qintptr descriptor)
{
	//Cannot open the connection if no connection was created
	if(socket)
		return false;

	//Create the socket and try to set the descriptor (check the parameter with the if clause)
	QSslSocket *socket = new QSslSocket(this);
	if(!socket->setSocketDescriptor(descriptor))
	{
		//The parameter was wrong, close the connection
		QMetaObject::invokeMethod(this, "closed", Qt::QueuedConnection);
		return false;
	}

	//Set the correct settings and start the connection
	socket->setLocalCertificate  ("sslcetificate.pem");
	socket->setPrivateKey	     ("sslkey.key");
	socket->setProtocol		     (QSsl::TlsV1_3);
	socket->startServerEncryption();

	//Set the socket as a member variable and set the connections
	initializeSocket(socket);

	return true;
}

bool Connection::open(QSslSocket *socket)
{
	//Normally, at this point, the socket cannot be null
	Q_ASSERT(socket);

	//Check if the socket is valid
	if(this->socket || !socket || !socket->isValid())
		return false;

	//Take ownership of the socket
	socket->setParent(this);

	//Set the socket as a member variable and set the connections
	initializeSocket(socket);

	return true;
}

bool Connection::open(const QHostAddress &address, quint16 port)
{
	//Check if the socket is valid
	if(this->socket || address.isNull())
		return false;

	//Create the socket, set its settings and start the connection
	QSslSocket *socket = new QSslSocket;
	socket->addCaCertificates	  ("sslcetificate.pem");
	socket->connectToHostEncrypted(address.toString(), port);

	//Set the socket as a member variable and set the connections
	initializeSocket(socket);

	return true;
}

void Connection::close()
{
	//Close the connection if th socket is valid
	if(socket && socket->isValid())
		socket->disconnectFromHost();
}

void Connection::send(const MessagePointer &messagePointer)
{
	//Normally, at this point, the socket cannot be null
	Q_ASSERT(socket);

	//Check if the socket is valid
	if(!socket->isValid())
		return;

	//Check if the message is valid
	const Message *message = messagePointer.data();
	if(!message)
		return;

	//Send the message as a json document
	QDataStream stream(socket);
	stream << QJsonDocument(message->toJson()).toJson();
}

void Connection::readData()
{
	QDataStream stream(socket);

	while(true)
	{
		//Read the JSON data
		stream.startTransaction();
		QByteArray jsonData;
		stream >> jsonData;
		if(!stream.commitTransaction())
			return; //The message may come in chunks, so wait for more

		//Parse the JSON data
		QJsonParseError parseError;
		const QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonData, &parseError);
		if(parseError.error != QJsonParseError::NoError)
		{
			emit statusReport(QStringLiteral("Error when parsing a received message: %1\nMessage: %2").arg(parseError.errorString()).arg(QString(jsonData)));
			return;
		}

		//Sanity check for the message
		if(!jsonDocument.isObject())
		{
			emit statusReport(QStringLiteral("JSON message is of unexpected type."));
			return;
		}

		//Convert the JSON data to a message object and emit the appropriate signal
		decodeJson(jsonDocument.object());
	}
}

void Connection::setLastError(QAbstractSocket::SocketError error)
{
	//Set the error string depending on the error type
	switch(error)
	{
		case QAbstractSocket::RemoteHostClosedError:
		case QAbstractSocket::ProxyConnectionClosedError:
			lastError = QStringLiteral("The host terminated the connection.");

			//Do not report this error, because it was reported in other functions already
			//(When clients disconnect or the server closes)
			return;

		case QAbstractSocket::ConnectionRefusedError:
			lastError = QStringLiteral("The host refused the connection.");
			break;

		case QAbstractSocket::ProxyConnectionRefusedError:
			lastError = QStringLiteral("The proxy refused the connection.");
			break;

		case QAbstractSocket::ProxyNotFoundError:
			lastError = QStringLiteral("Could not find the proxy.");
			break;

		case QAbstractSocket::HostNotFoundError:
			lastError = QStringLiteral("Could not find the server.");
			break;

		case QAbstractSocket::SocketAccessError:
			lastError = QStringLiteral("You don't have permissions to execute this operation.");
			break;

		case QAbstractSocket::SocketResourceError:
			lastError = QStringLiteral("Too many connections opened.");
			break;

		case QAbstractSocket::SocketTimeoutError:
			lastError = QStringLiteral("Operation timed out.");
			return;

		case QAbstractSocket::ProxyConnectionTimeoutError:
			lastError = QStringLiteral("Proxy timed out.");
			break;

		case QAbstractSocket::NetworkError:
			lastError = QStringLiteral("Unable to reach the network.");
			break;

		case QAbstractSocket::UnknownSocketError:
			lastError = QStringLiteral("An unknown error occured.");
			break;

		case QAbstractSocket::UnsupportedSocketOperationError:
			lastError = QStringLiteral("Operation not supported.");
			break;

		case QAbstractSocket::ProxyAuthenticationRequiredError:
			lastError = QStringLiteral("Your proxy requires authentication.");
			break;

		case QAbstractSocket::ProxyProtocolError:
			lastError = QStringLiteral("Proxy comunication failed.");
			break;

		case QAbstractSocket::TemporaryError:
		case QAbstractSocket::OperationError:
			lastError = QStringLiteral("Operation failed, please try again.");
			break;

		default:
			lastError = QStringLiteral("Non defined Error occured.");
			break;
	}

	//Print the error to the server window
	emit statusReport(lastError);
}

void Connection::initializeSocket(QSslSocket *socket)
{
	//Normally, at this point, the socket should be null, if it is null, set the member socket to the parameter
	Q_ASSERT(!this->socket);
	this->socket = socket;

	//Only act as if the connection was opened, when the encryption has actually begun.
	//When disconnected, close the connection (which in turn deletes the connection)
	//Read the data normally
	connect(socket, &QSslSocket::encrypted	 , this, &Connection::opened);
	connect(socket, &QSslSocket::disconnected, this, &Connection::closed);
	connect(socket, &QSslSocket::readyRead	 , this, &Connection::readData);

	//When an error occures, first set the last error string, after that, emmit the member error signal
	connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QSslSocket::error), this, &Connection::setLastError);
	connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QSslSocket::error), this, &Connection::error);

	//Delete the connection if it was closed
	connect(this, &Connection::closed, socket, &QObject::deleteLater);
}

void Connection::decodeJson(const QJsonObject &json)
{
	//Set the message depending on the json type
	MessagePointer message;
	switch(Message::getType(json))
	{
		case Message::LoginType:
			message = new MessageLogin();
			break;

		case Message::LoginStatusType:
			message = new MessageLoginStatus();
			break;

		case Message::LogoutType:
			message = new MessageLogout();
			break;

		case Message::TextType:
			message = new MessageText();
			break;

		default:
			//The message type has to be wrong
			emit statusReport(QStringLiteral("Received a message with an unknown type."));
			return;
	}

	//Check if the message is a json file
	if(!message->fromJson(json))
		emit statusReport(QStringLiteral("Could not decode the received JSON message."));
	else
		emit received(message); //Emit the received message
}
