# SecureChat
SecureChat is an open source, secure chatting application, that allows you to send messages without having to worry about your privacy.

# Create a Server
Just open the SecureChatServer application and press the start button.

![Server Window](https://i.ibb.co/qpMMPgn/Server-Window.jpg)

# Login
To login into a secure server, open the SecureChatClient application and enter the servers IP address and choose a name.

![Server Window](https://i.ibb.co/bzkgzKX/Login-Screen.jpg)

Afterwards you are free to chat.

![Server Window](https://i.ibb.co/68xDN0M/Chat-Window.jpg)

# Authors
**Adam Tumgaev** - *Everything, from programming to designing.*