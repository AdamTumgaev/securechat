#include "ServerWindow.hpp"
#include "Server.hpp"
#include "ui_ServerWindow.h"

#include <QPushButton>

ServerWindow::ServerWindow(QWidget *parent)
	: QWidget(parent)
	, ui	 (new Ui::ServerWindow)
{
	//Make window borderless and prepare ui
	setWindowFlag(Qt::FramelessWindowHint);
	ui->setupUi(this);

	//Remove the scroll bar
	ui->logEditor->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	//Connect the buttons to the corresponding functions
	connect(ui->startStopButton, &QPushButton::clicked, this, &ServerWindow::toggleServer);
	connect(ui->closeButton	   , &QPushButton::clicked, this, &ServerWindow::onCloseButtonClicked);
	connect(ui->minimizeButton , &QPushButton::clicked, this, &ServerWindow::onMinimizeButtonClicked);
}

ServerWindow::~ServerWindow()
{
	delete ui;
}

void ServerWindow::serverStarted()
{
	//Toggle the text on the button
	ui->startStopButton->setText(QLatin1String("Stop Server"));
}

void ServerWindow::serverStopped()
{
	//Toggle the text on the button
	ui->startStopButton->setText(QLatin1String("Start Server"));
}

void ServerWindow::logMessage(const QString &msg)
{
	//This function is called when some event happened somewhere, add the event to the log view
	ui->logEditor->appendPlainText(msg);
}

void ServerWindow::mousePressEvent(QMouseEvent *event)
{
	//Get the position of the mouse press
	mouseClickPositionX = event->x();
	mouseClickPositionY = event->y();
}

void ServerWindow::mouseMoveEvent(QMouseEvent *event)
{
	//Move the window just like the mouse is moving (follow the mouse movement)
	move(event->globalX() - mouseClickPositionX, event->globalY() - mouseClickPositionY);
}

void ServerWindow::onCloseButtonClicked()
{
	QApplication::quit();
}

void ServerWindow::onMinimizeButtonClicked()
{
	//Minimize the window
	QWidget::setWindowState(Qt::WindowMinimized);
}
