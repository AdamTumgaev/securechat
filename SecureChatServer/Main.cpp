#include "SecureChat.hpp"
#include "Server.hpp"
#include "ServerWindow.hpp"

#include <QApplication>
#include <QSslSocket>
#include <QMessageBox>

int main(int argc, char * argv[])
{
	QApplication application(argc, argv);

	//Check if SSL supported
	if(!QSslSocket::supportsSsl())
	{
		QMessageBox::critical(nullptr, "Secure Socket Client",
							  "This system does not support SSL/TLS.\n"
							  "Installing OpenSSL might help.");
		return -1;
	}

	//Open the server with the port that was defined with SECURE_CHAT_PORT
	Server server(SECURE_CHAT_PORT);

	//Open up the server window
	ServerWindow serverWindow;
	serverWindow.show();

	//Connect the button clicks and the logging into the window
	QObject::connect(&server, &Server::started     , &serverWindow, &ServerWindow::serverStarted);
	QObject::connect(&server, &Server::stopped     , &serverWindow, &ServerWindow::serverStopped);
	QObject::connect(&server, &Server::statusReport, &serverWindow, &ServerWindow::logMessage);

	//When stopping or toggling through the window, also stop or toggle the server
	QObject::connect(&application , &QApplication::aboutToQuit , &server, &Server::stop);
	QObject::connect(&serverWindow, &ServerWindow::toggleServer, &server, &Server::toggle);

	return QApplication::exec();
}
