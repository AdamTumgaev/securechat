#include "Server.hpp"
#include "Connection.hpp"
#include "Message.hpp"

#include <QDebug>

#include <functional>

Server::Server(quint16 port, QObject *parent)
	: QTcpServer(parent)
	, listenPort(port)
{
	//Register this type so it can be used in invokeMethod later
	qRegisterMetaType<qintptr>("qintptr");
}

quint16 Server::getPort() const
{
	return listenPort;
}

void Server::setPort(quint16 port)
{
	listenPort = port;
}

void Server::start()
{
	//Do not start if already started
	if(isListening())
		return;

	//Start listening for new connections and call and log this events
	if(listen(QHostAddress::Any, listenPort))
	{
		emit started	 ();
		emit statusReport(QStringLiteral("Server started."));
	}
	else //Could not start the server, log that event
		emit statusReport(QStringLiteral("Server failed to start!"));
}

void Server::stop()
{
	//Do not stop if already stopped
	if(!isListening())
		return;

	//Warn that the server will stop in a second
	emit aboutToStop();

	//Stop the server
	close();

	//Call and log the stopped event
	emit stopped	 ();
	emit statusReport(QStringLiteral("Server stopped."));
}

void Server::toggle()
{
	//Toggle the server state
	if(isListening())
		stop();
	else
		start();
}

void Server::closeSession(const QString &username)
{
	//Just remove the closing session from the participants list
	if(!participants.remove(username))
		return;

	//Log this event
	emit statusReport(QString("A client called '%1' disconnected.").arg(username));

	//Notify the other participants that a client has quitted
	MessagePointer logoutMessage(new MessageLogout());
	logoutMessage->setUsername(username);
	broadcast(logoutMessage);
}

void Server::processMessage(Connection *session, const MessagePointer &message)
{
	//Depending on the type of the message, act differently
	switch(message->getType())
	{
		//New user logged in
		case Message::LoginType:
		{
			//The message has to be a login message
			const MessageLogin *loginMessage = reinterpret_cast<const MessageLogin*>(message.data());

			QString username = loginMessage->getUsername();

			//Create a message that informs the client about the status of the login
			MessageLoginStatus *statusMessage = new MessageLoginStatus();
			statusMessage->setUsername(username);

			//Check if this username is not already taken
			if(participants.contains(username))
			{
				//Log to the server that a client connection was not accepted because the name was taken
				emit statusReport(QLatin1String("A clients connection is rejected (name already taken)."));

				//Login failed, also set the error information
				statusMessage->setStatus   (MessageLoginStatus::Fail);
				statusMessage->setErrorText(QStringLiteral("Username already in use by other user."));
			}
			else //Username available to take
			{
				//The login was successfull
				statusMessage->setStatus(MessageLoginStatus::Success);

				//Unregister the client if the connection is closed prematurely
				//NOTE: Use a blocking connection when we are threaded, because we may update something (e.g. the participants list)
				//In closeSession() thus we are subject to the object being destroyed before the update taking place.
				connect(session, &Connection::closed, this, std::bind(&Server::closeSession, this, username), session->thread() == thread() ? Qt::AutoConnection : Qt::BlockingQueuedConnection);

				//Connect all the existing clients to the new one
				for(ChatSessionHash::ConstIterator i = participants.constBegin(), end = participants.constEnd(); i != end; ++i)
				{
					connect(*i	   , &Connection::received, session, QOverload<const MessagePointer &>::of(&Connection::send));
					connect(session, &Connection::received, *i	   , QOverload<const MessagePointer &>::of(&Connection::send));
				}

				//Log to the server that a new user was accepted
				emit statusReport(QString("A client called '%1' connected.").arg(username));

				//Notify the other participants we have a new client connected
				MessagePointer loginMessage(new MessageLogin());
				loginMessage->setUsername(username);
				broadcast(loginMessage);

				//Insert the session into the active participants
				participants.insert(username, session);
			}

			send(session, MessagePointer(statusMessage));
			break;
		}
		case Message::LogoutType: //The user logged out from the server
		{
			//The message has to be a logout message
			const MessageLogout *logoutMessage = reinterpret_cast<const MessageLogout*>(message.data());

			//Get the connection for this user
			QString     username = logoutMessage->getUsername();
			Connection *session  = participants.value(username, nullptr);

			//The connection has to be valid, close the connection
			Q_ASSERT(session);
			QMetaObject::invokeMethod(session, "close", Qt::QueuedConnection);

			break;
		}
		default:; //Received a normal text message, do nothing
	}
}

void Server::broadcast(const MessagePointer &message)
{
	//Send the message to all the clients
	for(ChatSessionHash::ConstIterator i = participants.constBegin(), end = participants.constEnd(); i != end; ++i)
		send(i.value(), message);
}

void Server::send(Connection *session, const MessagePointer &message)
{
	//Send the message to this one client
	QMetaObject::invokeMethod(session, "send", Qt::QueuedConnection, Q_ARG(MessagePointer, message));
}

void Server::incomingConnection(qintptr handle)
{
	Connection *session = createSession();

	//Move the session to the correct thread
	session->setParent(nullptr);
	threadManager.moveObject(session);

	//Try to open the session in the correct thread
	bool opened;
	QMetaObject::invokeMethod(session, "open", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, opened), Q_ARG(qintptr, handle));

	//Log if the connection was successfull
	if(opened)
		emit statusReport(QStringLiteral("A client is trying to connect."));
	else
		emit statusReport(QStringLiteral("Could not initialize a clients session."));
}

Connection *Server::createSession()
{
	//Create a and initialize a user session
	Connection *session = new Connection(this);

	//Log the received messages and status (delegate the signals)
	connect(session, &Connection::statusReport, this, &Server::statusReport);
	connect(session, &Connection::received	  , this, &Server::messageReceived);
	connect(session, &Connection::received	  , this, std::bind(&Server::processMessage, this, session, std::placeholders::_1));

	//Take care of the cleaning up
	connect(this   , &Server::aboutToStop, session, &Connection::close);
	connect(session, &Connection::closed , session, &Connection::deleteLater);

	return session;
}
