#ifndef SERVER_HPP
#define SERVER_HPP

#include "SecureChat.hpp"
#include "ThreadManager.hpp"

#include <QTcpServer>
#include <QHash>

class Connection;

class Server : public QTcpServer
{
	Q_OBJECT
	Q_DISABLE_COPY(Server)

	typedef QHash<QString, Connection*> ChatSessionHash;

	public:
		explicit Server(quint16 = 0, QObject *parent = nullptr);

		void    setPort(quint16);
		quint16 getPort() const;

	public slots:
		void start ();
		void stop  ();
		void toggle();

		void broadcast(const MessagePointer &);

	signals:
		void started	();
		void aboutToStop();
		void stopped	();

		void statusReport	(const QString &);
		void messageReceived(const MessagePointer &);

	private slots:
		void closeSession(const QString &);

		void send		   (Connection *, const MessagePointer &);
		void processMessage(Connection *, const MessagePointer &);

	protected:
		void incomingConnection(qintptr handle) override;

		Connection *createSession();

	private:
		ThreadManager threadManager;

		quint16			listenPort;
		ChatSessionHash participants;
};

#endif //SERVER_HPP
