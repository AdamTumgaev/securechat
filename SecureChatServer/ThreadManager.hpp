#ifndef THREADMANAGER_HPP
#define THREADMANAGER_HPP

#include <QObject>
#include <QVector>
#include <QThread>

class ThreadManager : public QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(ThreadManager)

	public:
		explicit ThreadManager(int numberOfThreads = QThread::idealThreadCount(), QObject *parent = nullptr);
		~ThreadManager() override;

	public slots:
		void moveObject(QObject *);

	signals:
		void aboutToQuit();

	private:
		int indexOfLeastLoadedThread() const;

		QVector<QThread*>   threads;
		QVector<QAtomicInt> threadLoads;
};

#endif //THREADMANAGER_HPP
