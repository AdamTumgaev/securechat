#include "ThreadManager.hpp"

ThreadManager::ThreadManager(int numberOfThreads, QObject *parent)
	: QObject	 (parent)
	, threads	 (numberOfThreads)
	, threadLoads(numberOfThreads)
{
	//Create the correct amount of threads, start them and add the to the thread vector
	for(qint32 i = 0; i < numberOfThreads; i++)
	{
		QThread *thread = new QThread(this);
		thread->start();
		threads[i] = thread;
	}
}

ThreadManager::~ThreadManager()
{
	emit aboutToQuit();

	//Tell each of the threads it needs to quit
	for(QVector<QThread*>::ConstIterator i = threads.constBegin(), end = threads.constEnd(); i != end; ++i)
		(*i)->quit();

	//Wait for all the threads to actually quit before returning
	for(QVector<QThread*>::ConstIterator i = threads.constBegin(), end = threads.constEnd(); i != end; ++i)
		(*i)->wait();
}

void ThreadManager::moveObject(QObject *worker)
{
	//Find the index of the least loaded thread, the one with fewest worker objects
	int threadIndex = indexOfLeastLoadedThread();

	//Increase the worker count for this thread by one
	++threadLoads[threadIndex];

	//Connect the destruction of the object to decrement of load counter
	connect(worker, &QObject::destroyed, [this, threadIndex] () -> void
	{
		--threadLoads[threadIndex];
	});

	//Connect the workers deleteLater to the threads finished signal so we don't leak memory
	QThread *thread = threads[threadIndex];
	connect(thread, &QThread::finished, worker, &QObject::deleteLater);

	//Move the object to the least loaded worker thread
	worker->moveToThread(thread);
}

int ThreadManager::indexOfLeastLoadedThread() const
{
	int minIndex = 0;

	//Find the thread with the least amount of worker objects
	for(int i = 1, size = threads.size(); i < size; i++)
		if(threadLoads.at(i) < threadLoads.at(minIndex))
			minIndex = i;

	//Return the thread index with the least amount of worker objects
	return minIndex;
}
