#ifndef SERVERWINDOW_HPP
#define SERVERWINDOW_HPP

#include "SecureChat.hpp"

#include <QWidget>

namespace Ui { class ServerWindow; }

class ServerWindow : public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(ServerWindow)

	public:
		explicit ServerWindow(QWidget *parent = nullptr);
		~ServerWindow() override;

	public slots:
		void serverStopped();
		void serverStarted();

		void logMessage(const QString &);

		void onCloseButtonClicked   ();
		void onMinimizeButtonClicked();

	private:
		//For moving the window
		void mousePressEvent(QMouseEvent *event) override;
		void mouseMoveEvent (QMouseEvent *event) override;

	signals:
		void toggleServer();

	private:
		Ui::ServerWindow *ui;

		//For moving the window
		int mouseClickPositionX;
		int mouseClickPositionY;
};

#endif //SERVERWINDOW_HPP
