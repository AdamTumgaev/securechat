TEMPLATE = subdirs

SUBDIRS = SecureChat SecureChatClient SecureChatServer

SecureChatClient.depends = SecureChat
SecureChatServer.depends = SecureChat
