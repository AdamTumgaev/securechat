#include "ClientWindow.hpp"

#include <QApplication>
#include <QSslSocket>
#include <QMessageBox>

int main(int argc, char *argv[])
{
	QApplication application(argc, argv);

	//Check if SSL supported
	if(!QSslSocket::supportsSsl())
	{
		QMessageBox::critical(nullptr, "Secure Socket Client",
							  "This system does not support SSL/TLS.\n"
							  "Installing OpenSSL might help.");
		return -1;
	}

	ClientWindow mainWindow;
	mainWindow.show();

	return QApplication::exec();
}
