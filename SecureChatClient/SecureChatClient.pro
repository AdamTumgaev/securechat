include(../SecureChat.pri)

QT += core gui widgets network

TARGET   = SecureChatClient
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += $$PWD/../SecureChat
LIBS        += -L$$DESTDIR -lSecureChat

HEADERS += *.hpp
SOURCES += *.cpp
FORMS   += *.ui
