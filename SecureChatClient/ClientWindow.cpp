#include "ClientWindow.hpp"
#include "Message.hpp"
#include "ui_ClientWindow.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QHostAddress>
#include <QMouseEvent>
#include <QListView>
#include <QDebug>

#define CONNECT_PAGE_WIDTH  400
#define CONNECT_PAGE_HEIGHT 210
#define CHAT_PAGE_WIDTH     400
#define CHAT_PAGE_HEIGHT    500

ClientWindow::ClientWindow(QWidget *parent)
	: QWidget			 (parent)
	, connection		 (this)
	, ui				 (new Ui::ClientWindow)
	, chatItemModel		 (this)
	, mouseClickPositionX(0)
	, mouseClickPositionY(0)
{
	setupUi			();
	setupConnections();
}

ClientWindow::~ClientWindow()
{
	delete ui;
}

void ClientWindow::setupUi()
{
	//Make window borderless and prepare ui
	setWindowFlag(Qt::FramelessWindowHint);
	ui->setupUi(this);

	//Insert column to list view and make it read only
	chatItemModel.insertColumn	 (0);
	ui->chatView->setModel		 (&chatItemModel);
	ui->chatView->setFocusPolicy (Qt::NoFocus);
	ui->chatView->setEditTriggers(QAbstractItemView::NoEditTriggers);

	boldFont.setBold(true);
}

void ClientWindow::setupConnections()
{
	//Connect minimizing and closing the window
	connect(ui->closeButton	  , &QPushButton::clicked, this, &ClientWindow::onCloseButtonClicked);
	connect(ui->minimizeButton, &QPushButton::clicked, this, &ClientWindow::onMinimizeButtonClicked);
	connect(ui->backButton    , &QPushButton::clicked, this, &ClientWindow::onBackButtonClicked);

	//Connect address input to validator function
	connect(ui->addressEdit, &QLineEdit::textEdited, this, &ClientWindow::checkIfInputValid);
	connect(ui->nameEdit   , &QLineEdit::textEdited, this, &ClientWindow::checkIfInputValid);

	//When trying to connect to server
	connect(ui->connectButton, &QPushButton::clicked, this, &ClientWindow::attemptConnection);

	//Succeeded or failed connecting
	connect(&connection, &Connection::opened, this, &ClientWindow::connectedToServer);
	connect(&connection, &Connection::error , this, &ClientWindow::error);

	//Sending and receiving messages
	connect(ui->sendButton , &QPushButton::clicked	  , this, &ClientWindow::sendMessage);
	connect(ui->messageEdit, &QLineEdit::returnPressed, this, &ClientWindow::sendMessage);
	connect(&connection    , &Connection::received    , this, &ClientWindow::messageReceived);
}

void ClientWindow::attemptConnection()
{
	//If not connected, try connect
	if(connection.getState() == QAbstractSocket::UnconnectedState)
	{
		ui->connectPage->setEnabled(false);
		ui->errorLabel ->clear     ();

		//Tell the client to connect to the host using the port defined by SECURE_CHAT_PORT
		connection.open(QHostAddress(ui->addressEdit->text()), SECURE_CHAT_PORT);
	}
	else
		connectedToServer(); //Already connected, only send user name
}

void ClientWindow::connectedToServer()
{
	//Reset the username
	username = ui->nameEdit->text();
	lastUserName.clear();

	//Log into the server with the selected user name
	MessageLogin *message = new MessageLogin();
	message->setUsername(username);
	connection.send(MessagePointer(message));
}

void ClientWindow::sendMessage()
{
	const QString messageText = ui->messageEdit->text();

	//If the user didn't type anything just ignore the whole thing
	if(messageText.isEmpty())
		return;

	//We send the message that the user typed
	MessageText *message = new MessageText();
	message->setUsername(username);
	message->setText	(messageText);
	connection.send(MessagePointer(message));

	//Now we add our own message to the list
	addMessage(messageText, Qt::AlignRight | Qt::AlignVCenter);

	//Remove the text from the input area
	ui->messageEdit->clear();
}

void ClientWindow::error()
{
	//Act as if back button pressed (show connect page and etc.) and show the error
	onBackButtonClicked();
	QMessageBox::critical(this, QLatin1String("Connection Error"), connection.getLastError());
}

void ClientWindow::messageReceived(const MessagePointer &message)
{
	//Make sure that the message has data
	Q_ASSERT(message.data());

	//Depending on the type of the message, call specific functions
	switch(message->getType())
	{
		case Message::LoginType:
			logInReceived(*reinterpret_cast<const MessageLogin*>(message.data()));
			return;

		case Message::LoginStatusType:
			logInStatusReceived(*reinterpret_cast<const MessageLoginStatus*>(message.data()));
			return;

		case Message::LogoutType:
			logOutReceived(*reinterpret_cast<const MessageLogout*>(message.data()));
			return;

		case Message::TextType:
			textReceived(*reinterpret_cast<const MessageText*>(message.data()));
			return;

		default:
			return;
	}
}

void ClientWindow::logInReceived(const MessageLogin &message)
{
	//Reset the last username and show that new user connected
	lastUserName.clear();
	addMessage(QString("%1 joined the Chat.").arg(message.getUsername()), Qt::AlignCenter);
}

void ClientWindow::logInStatusReceived(const MessageLoginStatus &message)
{
	//No matter whether name was accepted or not, enable the connection page again
	//(because page will only be switched if name accepted)
	ui->connectPage->setEnabled(true);

	//If name was accepted, show chat page
	if(message.getStatus() == MessageLoginStatus::Success)
	{
		//Resize and show the chat
		resize(CHAT_PAGE_WIDTH, CHAT_PAGE_HEIGHT);
		ui->stackedWidget->setCurrentIndex(1);
		ui->backButton   ->setEnabled     (true);
		addMessage(QLatin1String("You joined the Chat."), Qt::AlignCenter);

		ui->errorLabel->clear();
		lastUserName   .clear();
	}
	else //Name was not accepted, display the error
		ui->errorLabel->setText(message.getErrorText());
}

void ClientWindow::logOutReceived(const MessageLogout & message)
{
	//Reset the last username and show that a user disconnected
	lastUserName.clear();
	addMessage(QString("%1 left the Chat.").arg(message.getUsername()), Qt::AlignCenter);
}

void ClientWindow::textReceived(const MessageText & message)
{
	//If name of this user was not displayed yet, display it
	if(message.getUsername() != lastUserName)
	{
		lastUserName = message.getUsername();
		addMessage(QStringLiteral("[%1]").arg(lastUserName), Qt::AlignLeft | Qt::AlignVCenter, true);
	}

	//Display the actual message
	addMessage(message.getText(), Qt::AlignLeft | Qt::AlignVCenter);
}

void ClientWindow::addMessage(const QString &data, Qt::Alignment alignment, bool bold)
{
	//Add a new row
	const int iNextRow = chatItemModel.rowCount();
	chatItemModel.insertRow(iNextRow);
	QModelIndex qNextRow = chatItemModel.index(iNextRow, 0);

	//Style the message
	chatItemModel.setData(qNextRow, data);
	chatItemModel.setData(qNextRow, QVariant::fromValue<int>(alignment), Qt::TextAlignmentRole);
	chatItemModel.setData(qNextRow, QBrush(Qt::white), Qt::ForegroundRole);

	//Use bold font for bold messages
	if(bold)
		chatItemModel.setData(qNextRow, boldFont, Qt::FontRole);

	//Always show the newest message
	ui->chatView->scrollToBottom();
}

void ClientWindow::mousePressEvent(QMouseEvent *event)
{
	//Get the position of the mouse press
	mouseClickPositionX = event->x();
	mouseClickPositionY = event->y();
}

void ClientWindow::mouseMoveEvent(QMouseEvent *event)
{
	//Move the window just like the mouse is moving (follow the mouse movement)
	move(event->globalX() - mouseClickPositionX, event->globalY() - mouseClickPositionY);
}

void ClientWindow::onCloseButtonClicked()
{
	QApplication::quit();
}

void ClientWindow::onMinimizeButtonClicked()
{
	//Minimize the window
	QWidget::setWindowState(Qt::WindowMinimized);
}

void ClientWindow::onBackButtonClicked()
{
	//Close the connection with the server
	connection.close();

	//Resize and show the connect page
	resize(CONNECT_PAGE_WIDTH, CONNECT_PAGE_HEIGHT);
	ui->stackedWidget->setCurrentIndex(0);

	//Make sure that connect page is enabled and there is no back button
	ui->connectPage->setEnabled(true);
	ui->backButton ->setEnabled(false);

	//Reset the chat page
	ui->messageEdit->clear		 ();
	chatItemModel   .clear		 ();
	lastUserName    .clear		 ();
	chatItemModel   .insertColumn(0);
}

void ClientWindow::checkIfInputValid(const QString &)
{
	//Make sure that only IPv4 addresses are entered
	ui->connectButton->setEnabled(QRegExp("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}")
	.exactMatch(ui->addressEdit->text()) && !ui->nameEdit->text().isEmpty());
}
