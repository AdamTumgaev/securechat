#ifndef CLIENTWINDOW_HPP
#define CLIENTWINDOW_HPP

#include "SecureChat.hpp"
#include "Connection.hpp"

#include <QWidget>
#include <QStandardItemModel>

namespace Ui { class ClientWindow; }

class MessageLogin;
class MessageLoginStatus;
class MessageLogout;
class MessageText;

class QSslSocket;

class ClientWindow : public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(ClientWindow)

	public:
		explicit ClientWindow(QWidget *parent = nullptr);
		~ClientWindow() override;

	private:
		void setupUi		 ();
		void setupConnections();

		void logInReceived (const MessageLogin  &);
		void logOutReceived(const MessageLogout &);

		void logInStatusReceived(const MessageLoginStatus &);
		void textReceived		(const MessageText		  &);

		void addMessage(const QString &, Qt::Alignment, bool bold = false);

		//For moving the window
		void mousePressEvent(QMouseEvent *event) override;
		void mouseMoveEvent (QMouseEvent *event) override;

	private slots:
		void attemptConnection();
		void connectedToServer();

		void sendMessage    ();
		void messageReceived(const MessagePointer &);

		void error();

		void onCloseButtonClicked   ();
		void onMinimizeButtonClicked();
		void onBackButtonClicked	();

		void checkIfInputValid(const QString &);

	private:
		Connection   connection;
		QSslSocket  *socket;

		Ui::ClientWindow   *ui;
		QStandardItemModel  chatItemModel;
		QFont				boldFont;

		QString username;
		QString lastUserName;

		//For moving the window
		int mouseClickPositionX;
		int mouseClickPositionY;
};

#endif //CLIENTWINDOW_HPP
